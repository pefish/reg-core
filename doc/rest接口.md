## 账户

#### 获取账户信息  get  /account/{userId}

#### 获取指定账户的所有接收的交易  get  /account/received_txs/{userId}

#### 获取指定账户的所有发出的交易  get  /account/sended_txs/{userId}

## 交易

#### 根据hash获取交易详情  get  /transaction/{tx_hash}

#### 构造原生交易  post  /transaction/create

#### 签名交易  post  /transaction/sign

#### 广播交易  post  /transaction/broadcast

## 块

#### 获取当前最新块信息  get  /block/latest

#### 根据块hash查看块信息  get  /block/{block_hash}

#### 获取块  get  /block/list_blocks  [done]

## 网络

#### 添加邻节点  post  /network/add_peer  [done]
#### 查看所有邻节点  get  /network/list_peers  [done]

## 命令

#### 向客户端发送命令  post  /command/send_client  [done]
#### 向服务端发送命令  post  /command/send_server  [done]

## 区块链

#### 获取本节点相关信息  get  /chain/get_info  [done]
