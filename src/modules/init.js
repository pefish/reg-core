/**
 * Created by joy on 26/09/2017.
 */
import yargs from 'yargs'
import sqlite from 'js-common-modules/lib/plugins/sqlite'
import log4js from 'js-common-modules/lib/plugins/log4js'
import path from 'path'
import FileUtil from 'js-common-modules/lib/utils/FileUtil'
import SocketServerHelper from 'js-common-modules/lib/helpers/SocketServerHelper'
import SocketClientHelper from 'js-common-modules/lib/helpers/SocketClientHelper'
import Account from '../modules/Account'
import Block from '../modules/Block'
import Peer from '../modules/Peer'

function initDirs () {
  FileUtil.mkdirSync(global['args']['dataDir'] + '/temp') && (global['dirs']['temp'] = global['args']['dataDir'] + '/temp')
  FileUtil.mkdirSync(global['args']['dataDir'] + '/log') && (global['dirs']['log'] = global['args']['dataDir'] + '/log')
  FileUtil.mkdirSync(global['args']['dataDir'] + '/db') && (global['dirs']['db'] = global['args']['dataDir'] + '/db')
}

export default async () => {
  global['dirs'] = {}
  const userConfigFile = yargs.option('config', {
    demand: false,
    describe: 'config file',
    type: 'string'
  }).argv['config']
  const userConfig = userConfigFile ? FileUtil.loadFromJsonFile(`${userConfigFile}`) : require('../constants/defaultConfig.js').default
  global['args'] = yargs
  .help('h')
  .alias('h', 'help')
  .usage('Usage: regd [options]')
  .option('sync_port', {
    alias : 'syncPort',
    demand: true,
    default: userConfig['network']['sync_port'],
    describe: 'port',
    type: 'number'
  })
  .option('server', {
    demand: true,
    default: userConfig['network']['server'],
    describe: 'enable server',
    type: 'boolean'
  })
  .option('rest_user', {
    alias : 'restUser',
    demand: true,
    default: userConfig['rest']['rest_user'],
    describe: 'rest user name',
    type: 'string'
  })
  .option('rest_pass', {
    alias : 'restPass',
    demand: true,
    default: userConfig['rest']['rest_pass'],
    describe: 'rest user password',
    type: 'string'
  })
  .option('rest_port', {
    alias : 'restPort',
    demand: true,
    default: userConfig['rest']['rest_port'],
    describe: 'rest port',
    type: 'number'
  })
  .option('rest_host', {
    alias : 'restHost',
    demand: true,
    default: userConfig['rest']['rest_host'],
    describe: 'rest host',
    type: 'string'
  })
  .option('rest_origins', {
    alias : 'restOrigins',
    demand: true,
    default: userConfig['rest']['rest_origins'],
    describe: 'rest origins',
    type: 'string'
  })
  .option('data_dir', {
    alias : 'dataDir',
    demand: true,
    default: userConfig['system']['data_dir'],
    describe: 'data dir',
    type: 'string'
  })
  .option('peers', {
    demand: true,
    default: userConfig['network']['peers'],
    describe: 'peers',
    type: 'array'
  })
  .example('regd --sync_port 30303', 'socket监听30303，开启rest')
  .epilog('copyright 2017')
    .argv
  global['args']['userConfig'] = userConfig
  initDirs()
  global['logger'] = await log4js(global['dirs']['log'])
  global['sqliteHelper'] = await sqlite(global['dirs']['db'] + '/db.mdb', false)
  global['socketServerHelper'] = new SocketServerHelper(path.join(__dirname, '../serverSocketRoutes'), path.join(__dirname, '../serverSockets'))
  global['socketClientHelper'] = new SocketClientHelper(path.join(__dirname, '../clientSocketRoutes'), path.join(__dirname, '../clientSockets'))
  global['accountHelper'] = new Account()
  global['blockHelper'] = new Block()
  global['peerHelper'] = new Peer()
}
