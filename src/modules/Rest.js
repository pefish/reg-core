/**
 * Created by joy on 25/09/2017.
 */

import HttpServerHelper from 'js-common-modules/lib/helpers/HttpServerHelper'
import path from 'path'

export default class Rest {
  async listen (port) {
    const httpServerHelper = new HttpServerHelper()
    await httpServerHelper.listen(port, path.join(__dirname, '../apiRoutes'), path.join(__dirname, '../apis'), args['restOrigins'])
  }
}
