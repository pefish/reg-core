/**
 * Created by joy on 25/09/2017.
 */

export default [
  {
    path: '/chain/get_info',
    method: 'get',
    preHandlers: [

    ],
    apiHandler: 'getInfo',
    expects: [
      {
        result: {
          succeed: [
            ['is', true]
          ],
          config: [
            ['notEmpty']
          ],
          chain: [
            ['notEmpty']
          ],
        }
      }
    ]
  },
]
