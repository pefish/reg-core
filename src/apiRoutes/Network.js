/**
 * Created by joy on 25/09/2017.
 */

export default [
  {
    path: '/network/add_peer',
    method: 'post',
    preHandlers: [
      'basicAuth',
      'apiParamsValidate'
    ],
    apiHandler: 'addPeer',
    params: {
      url: {
        policies: [
          ['notEmpty', true],
        ]
      }
    },
    expects: [
      {
        headers: {
          Authorization: 'Basic dGVzdDp0ZXN0'
        },
        params: {
          url: 'http://localhost:3241'
        },
        result: {
          succeed: [
            ['is', true]
          ]
        }
      }
    ]
  },
  {
    path: '/network/list_peers',
    method: 'get',
    preHandlers: [
      'basicAuth',
      'apiParamsValidate'
    ],
    apiHandler: 'listPeers',
    expects: [
      {
        headers: {
          Authorization: 'Basic dGVzdDp0ZXN0'
        },
        result: {
          succeed: [
            ['is', true]
          ],
          peers: [
            ['notEmpty']
          ]
        }
      }
    ]
  }
]
