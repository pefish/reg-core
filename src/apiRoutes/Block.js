/**
 * Created by joy on 25/09/2017.
 */
import errorCodes from '../constants/errorCodes'

export default [
  {
    path: '/block/list_blocks',
    method: 'get',
    preHandlers: [
      'apiParamsValidate'
    ],
    apiHandler: 'listBlocks',
    params: {
      start: {
        policies: [
          ['default', 0],
          ['isType', 'integer']
        ]
      },
      limit: {
        policies: [
          ['default', 3000],
          ['isType', 'integer']
        ]
      }
    },
    expects: [
      {
        params: {
          start: 1,
          limit: 10
        },
        result: {
          succeed: [
            ['is', true]
          ],
          blocks: [
            ['notEmpty']
          ]
        }
      },
      {
        params: {
          start: 'a',
          limit: 10
        },
        result: {
          succeed: [
            ['is', false]
          ],
          error_code: [
            ['is', errorCodes.PARAM_TYPE_ERROR]
          ]
        }
      }
    ]
  },
]
