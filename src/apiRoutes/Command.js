/**
 * Created by joy on 25/09/2017.
 */
import errorCodes from '../constants/errorCodes'

export default [
  {
    path: '/command/send_client',
    method: 'post',
    preHandlers: [
      'basicAuth',
      'apiParamsValidate'
    ],
    apiHandler: 'sendCommandToClient',
    params: {
      client_id: {
        policies: [
          ['notEmpty', true],
        ]
      },
      event: {
        policies: [
          ['notEmpty', true],
        ]
      },
      data: {
        policies: [

        ]
      }
    },
    expects: [
      {
        headers: {
          Authorization: 'Basic dGVzdDp0ZXN0'
        },
        params: {
          client_id: 'client not exist',
          event: 'test'
        },
        result: {
          succeed: [
            ['is', false]
          ],
          error_code: [
            ['is', errorCodes.CLIENT_NOT_EXIST]
          ]
        }
      }
    ]
  },
  {
    path: '/command/send_server',
    method: 'post',
    preHandlers: [
      'basicAuth',
      'apiParamsValidate'
    ],
    apiHandler: 'sendCommandToServer',
    params: {
      url: {
        policies: [
          ['notEmpty', true],
        ]
      },
      event: {
        policies: [
          ['notEmpty', true],
        ]
      },
      data: {
        policies: [

        ]
      }
    },
    expects: [
      {
        headers: {
          Authorization: 'Basic dGVzdDp0ZXN0'
        },
        params: {
          url: 'http://localhost:4222',
          event: 'test'
        },
        result: {
          succeed: [
            ['is', false]
          ],
          error_code: [
            ['is', errorCodes.SERVER_NOT_EXIST]
          ]
        }
      }
    ]
  },
]
