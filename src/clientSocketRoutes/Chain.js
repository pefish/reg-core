/**
 * Created by joy on 25/09/2017.
 */

export default [
  {
    event: 'res_chain_get_info',
    handler: 'getInfo',
  },
]
