/**
 * Created by joy on 25/09/2017.
 */
import Peer from '../modules/Peer'

export default class Network {
  async addPeer (req) {
    const { url } = req.body
    const peer = new Peer()
    peer.connectPeer(url, (url) => {
      peerHelper.removePeer(url, 'server')
    }).then((result) => {
      peerHelper.addPeer(url, result['serverInfo'], 'server')
    })
    return {}
  }

  async listPeers () {
    // logger.error(socketServerHelper.Clients)
    return {
      peers: GlobalVars.getInstance().Peers
    }
  }
}
