/**
 * Created by joy on 25/09/2017.
 */

export default class Block {
  async listBlocks (req) {
    const { start, limit } = req['query']
    return {
      blocks: await blockHelper.getBlocks(start, limit)
    }
  }
}
