/**
 * Created by joy on 25/09/2017.
 */

export default class Command {
  async sendCommandToClient (req) {
    const { client_id, event, data } = req.body
    await socketServerHelper.emit(client_id, event, data)
    return {}
  }

  async sendCommandToServer (req) {
    const { url, event, data } = req.body
    await socketClientHelper.emit(url, event, data)
    return {}
  }

}
