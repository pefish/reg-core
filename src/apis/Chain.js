/**
 * Created by joy on 25/09/2017.
 */

export default class Chain {
  async getInfo (req) {
    return {
      config: args['userConfig'],
      chain: {
        latestHeight: await blockHelper.getLocalLatestHeight()
      }
    }
  }
}
