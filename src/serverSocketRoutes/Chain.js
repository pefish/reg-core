/**
 * Created by joy on 25/09/2017.
 */
import path from 'path'

export default [
  {
    event: 'req_chain_get_info',
    handlerPath: path.join(__dirname, '../apis/Chain'),
    handler: 'getInfo',
    resEvent: 'res_chain_get_info'
  },
]
