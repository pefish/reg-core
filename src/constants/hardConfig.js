
export default {
  geneBlockInterval: 60000,
  syncInterval: 10000,
  coinbaseDelegators: [
    {
      path: 'm/0/0',
      address: 'mgWwKiBEFBi8hzwQ3uvc3JGnJFaZnXVS5f',
      alias: 'coinbase'
    }
  ]
}
