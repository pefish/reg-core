import path from 'path'
import os from 'os'

export default {
  network: {
    peers: [
      {
        socket: "http://localhost:30304",
        rest: "http://localhost:8546"
      }
    ],
    sync_port: 30303,
    server: false
  },
  rest: {
    rest_user: "test",
    rest_pass: "test",
    rest_port: 8545,
    rest_host: "localhost",
    rest_origins: "*"
  },
  system: {
    data_dir: path.join(os.homedir(), '.reg_core')
  }
}
