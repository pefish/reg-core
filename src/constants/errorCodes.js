import errorCodes from 'js-common-modules/lib/constants/errorCodes'

export default {
  ...errorCodes,
}
